swagger: '2.0'
info:
  description: Simple API
  version: 0.0.1
  title: Application API
tags:
  - name: products
    description: Used in frontend to get all products available
  - name: products-names
    description: Used in frontend to get all products available by names only
  - name: products-suggest
    description: Used in frontend to get all products that match query for suggestions
  - name: recipes-search
    description: used in frontend to post products chosen by user to get recipes that match them
  - name: sources
    description: Used in front end to get all sources available
  - name: products-save
    description: Post new product to save
  - name: products-save-name
    description: Post new product to save by name only
  - name: recipes-save
    description: Post new recipe to save
  - name: sources-internal
    description: Used in back end to get all sources available
  - name: sources-save
    description: Post new source to save
schemes:
  - http
paths:
  /api/products/all:
    get:
      tags:
        - products
      summary: Get all products
      description: Multiple status values can be provided with comma separated strings
      operationId: getAllProducts
      produces:
        - application/json
      parameters: []
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/ProductsList'
        '400':
          description: Invalid status value
  /api/products/all/names:
    get:
      tags:
        - products-names
      summary: Get all products names
      description: Multiple status values can be provided with comma separated strings
      operationId: getAllProductsNames
      produces:
        - application/json
      parameters: []
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/ProductsNameList'
        '400':
          description: Invalid status value
  /api/products/suggest/{query}:
    get:
      tags:
        - products-suggest
      summary: Get suggestions of products matching query
      description: Multiple status values can be provided with comma separated strings
      operationId: getProductsSuggestions
      produces:
        - application/json
      parameters:
        - in: path
          name: query
          type: string
          required: true
          description: Query for suggestions lookup
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/ProductsNameList'
        '400':
          description: Invalid status value
  /_internal/products:
    post:
      tags:
        - products-save
      summary: Save new product
      description: Multiple status values can be provided with comma separated strings
      operationId: postNewProduct
      parameters:
        - in: body
          description: Product to save
          required: true
          name: Product
          schema:
            $ref: '#/definitions/Product'
      responses:
        '201':
          description: product saved successfully
          schema:
            $ref: '#/definitions/Product'
        '409':
          description: product with that name already exists
          schema:
            $ref: '#/definitions/Product'
        '400':
          description: bad request, missing required fields
  /_internal/products/name:
    post:
      tags:
        - products-save-name
      summary: Save new product by name
      description: Multiple status values can be provided with comma separated strings
      operationId: postNewProductName
      parameters:
        - in: body
          description: Product to save
          required: true
          name: Product
          schema:
            type: string
      responses:
        '201':
          description: product saved successfully
          schema:
            $ref: '#/definitions/Product'
        '409':
          description: product with that name already exists
          schema:
            $ref: '#/definitions/Product'
        '400':
          description: bad request, missing required fields
  /api/recipes/search:
    post:
      tags:
        - recipes-search
      summary: Get recipes list by giving products names
      description: Multiple status values can be provided with comma separated strings
      operationId: getRecipes
      produces:
        - application/json
      parameters:
        - in: body
          description: Products names list
          required: true
          name: Products list
          schema:
            $ref: '#/definitions/ProductsNameList'
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/RecipesList'
        '400':
          description: bad request, products list is empty
  /_internal/recipes:
    post:
      tags:
        - recipes-save
      summary: Save new recipe
      description: Multiple status values can be provided with comma separated strings
      operationId: postNewRecipe
      parameters:
        - in: body
          description: Recipe description
          required: true
          name: Recipe
          schema:
            $ref: '#/definitions/Recipe'
      responses:
        '201':
          description: recipe saved successfully
          schema:
            $ref: '#/definitions/Recipe'
        '409':
          description: recipe with that name and sourceId already exists
          schema:
            $ref: '#/definitions/Recipe'
        '400':
          description: bad request, missing required fields
  /api/sources/all:
    get:
      tags:
        - sources
      summary: Get all sources
      description: Multiple status values can be provided with comma separated strings
      operationId: getAllSources
      produces:
        - application/json
      parameters: []
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/SourcesList'
        '400':
          description: Invalid status value
  /_internal/sources/all:
    get:
      tags:
        - sources-internal
      summary: Get all sources
      description: Multiple status values can be provided with comma separated strings
      operationId: getAllSourcesInternal
      produces:
        - application/json
      parameters: []
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/SourcesList'
        '400':
          description: Invalid status value
  /_internal/sources:
    post:
      tags:
        - sources-save
      summary: Save new source
      description: Multiple status values can be provided with comma separated strings
      operationId: postNewSource
      parameters:
        - in: body
          description: Source description
          required: true
          name: Source
          schema:
            $ref: '#/definitions/Source'
      responses:
        '201':
          description: source saved successfully
          schema:
            $ref: '#/definitions/Source'
        '409':
          description: source with that name already exists
          schema:
            $ref: '#/definitions/Source'
        '400':
          description: bad request, missing required fields
definitions:
  Source:
    type: object
    required: [name, url]
    properties:
      name:
        type: string
      url:
        type: string
  SourcesList:
    type: object
    required: [sources]
    properties:
      sources:
        type: array
        items:
          $ref: '#/definitions/Source'
      length:
        type: number
  ProductsList:
    type: object
    required: [products]
    properties:
      products:
        type: array
        items:
          $ref: '#/definitions/Product'
      length:
        type: number
  ProductsNameList:
    type: object
    required: [products]
    properties:
      products:
        type: array
        items:
          type: string
      length:
        type: number
  Product:
    type: object
    required: [name]
    properties:
      name:
        type: string
      value:
        type: string
  Recipe:
    type: object
    required: [name, url, products]
    properties:
      id:
        type: integer
      sourceId:
        type: string
      source:
        $ref: '#/definitions/Source'
      name:
        type: string
      url:
        type: string
      photoUrl:
        type: string
      products:
        type: array
        items:
          $ref: '#/definitions/Product'
  RecipesList:
    type: object
    required: [recipes]
    properties:
      recipes:
        type: array
        items:
          $ref: '#/definitions/Recipe'
      length:
        type: number
