#!/bin/bash

docker run --rm \  
  -p 443:443 -p 80:80 --name letsencrypt \
  -v "/etc/letsencrypt:/etc/letsencrypt" \
  -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
  certbot/certbot certonly -n \
  -m "frondeus@gmail.com" \
  -d bitefor.me \
  -d www.bitefor.me \
  --standalone --agree-tos
