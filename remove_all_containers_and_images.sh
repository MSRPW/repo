#!/bin/bash

docker rm --force $(docker ps -a -q)
# docker rmi --force $(docker images -q)
docker rmi --force $(docker images -q | grep "^<none>" | awk "{print $3}") 
