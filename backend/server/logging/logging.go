package logging

import (
	"fmt"
	"log"
	"os"
	"runtime"
)

type Level uint32

const (
	DebugLevel Level = iota
	InfoLevel
	WarningLevel
	ErrorLevel
	SilentLevel
)

type Logger struct {
	info    *log.Logger
	warning *log.Logger
	error   *log.Logger
	debug   *log.Logger
	level   Level
}

func New() *Logger {
	return &Logger{
		info:    log.New(os.Stdout, "[INFO] ", log.LstdFlags),
		warning: log.New(os.Stdout, "[WARNING] ", log.LstdFlags),
		error:   log.New(os.Stdout, "[ERROR] ", log.LstdFlags|log.Lmicroseconds),
		debug:   log.New(os.Stdout, "[DEBUG] ", log.LstdFlags|log.Lmicroseconds),
		level:   ErrorLevel,
	}
}

func (l *Logger) SetLevel(level Level) {
	l.level = level
}

func (l *Logger) Level() Level {
	return l.level
}

func (l *Logger) SetPrefix(prefix string) {
	l.info.SetPrefix(fmt.Sprintf("%s%s", prefix, l.info.Prefix()))
	l.warning.SetPrefix(fmt.Sprintf("%s%s", prefix, l.warning.Prefix()))
	l.error.SetPrefix(fmt.Sprintf("%s%s", prefix, l.error.Prefix()))
	l.debug.SetPrefix(fmt.Sprintf("%s%s", prefix, l.debug.Prefix()))
}

func (l *Logger) Info(msg string) {
	if l.level <= InfoLevel {
		l.info.Println(msg)
	}
}

func (l *Logger) Infof(format string, a ...interface{}) {
	if l.level <= InfoLevel {
		l.info.Printf(format, a...)
	}
}

func (l *Logger) Warning(msg string) {
	if l.level <= WarningLevel {
		l.warning.Println(msg)
	}
}

func (l *Logger) Warningf(format string, a ...interface{}) {
	if l.level <= WarningLevel {
		l.warning.Printf(format, a...)
	}
}

func (l *Logger) Error(msg string) {
	if l.level <= ErrorLevel {
		_, file, no, ok := runtime.Caller(1)
		if ok {
			l.error.Printf("%s:%d: %s\n", file, no, msg)
		} else {
			l.error.Println(msg)
		}
	}
}

func (l *Logger) Errorf(format string, a ...interface{}) {
	if l.level <= ErrorLevel {
		_, file, no, ok := runtime.Caller(1)
		if ok {
			l.error.Printf("%s:%d: %s\n", file, no, fmt.Sprintf(format, a...))
		} else {
			l.error.Printf(format, a...)
		}
	}
}

func (l *Logger) Debug(msg string) {
	if l.level == DebugLevel {
		_, file, no, ok := runtime.Caller(1)
		if ok {
			l.debug.Printf("%s:%d: %s\n", file, no, msg)
		} else {
			l.debug.Println(msg)
		}
	}
}

func (l *Logger) Debugf(format string, a ...interface{}) {
	if l.level == DebugLevel {
		_, file, no, ok := runtime.Caller(1)
		if ok {
			l.debug.Printf("%s:%d: %s\n", file, no, fmt.Sprintf(format, a...))
		} else {
			l.debug.Printf(format, a...)
		}
	}
}
