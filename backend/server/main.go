package main

import (
	goctx "context"

	"github.com/kataras/iris"

	"strconv"

	"os"

	"strings"

	elasticsource "gitlab.com/msrpw/repo/backend/server/datasource"
	"gitlab.com/msrpw/repo/backend/server/logging"
	"gitlab.com/msrpw/repo/backend/server/repositories"
	"gitlab.com/msrpw/repo/backend/server/services"
	controllers "gitlab.com/msrpw/repo/backend/server/web/apicontrollers"
	"gitlab.com/msrpw/repo/backend/server/web/intercontrollers"
)

const (
	ApiPrefix      = "api"
	InternalPrefix = "_internal"
)

func buildPath(a ...string) string {
	return strings.Join(append([]string{"/"}, a...), "/")
}

func isDebug() bool {
	debugEnv, ok := os.LookupEnv("DEBUG")
	if ok {
		debug, err := strconv.ParseBool(debugEnv)
		if err == nil && debug {
			return true
		}
	}
	return false
}

func main() {

	var app *iris.Application
	{
		app = iris.New()
		app.Logger().SetOutput(os.Stdout)
		app.Logger().SetLevel("warning")
		app.Logger().SetPrefix("[iris]")
	}

	var logger *logging.Logger
	{
		logger = logging.New()
		logger.SetPrefix("[server]")
		if isDebug() {
			logger.SetLevel(logging.DebugLevel)
		} else {
			logger.SetLevel(logging.InfoLevel)
		}
	}

	var esClient *elasticsource.ElasticClient
	{
		client, err := elasticsource.NewClient(
			goctx.Background(),
			logger,
			"recipefinder",
			elasticsource.RecipeFinderMapping)
		if err != nil {
			logger.Warning(err.Error())
		}
		esClient = client
	}

	{
		productRepository := repositories.NewProductRepository(esClient)
		productService := services.NewProductService(productRepository, logger)
		app.Controller(
			buildPath(ApiPrefix, "products"),
			new(controllers.ProductController),
			productService,
		)
		app.Controller(
			buildPath(InternalPrefix, "products"),
			new(intercontrollers.ProductController),
			productService,
		)

		sourceRepository := repositories.NewSourceRepository(esClient)
		sourceService := services.NewSourceService(sourceRepository, logger)
		app.Controller(
			buildPath(ApiPrefix, "sources"),
			new(controllers.SourceController),
			sourceService,
		)
		app.Controller(
			buildPath(InternalPrefix, "sources"),
			new(intercontrollers.SourceController),
			sourceService,
		)

		recipeRepository := repositories.NewRecipeRepository(esClient)
		recipeService := services.NewRecipeService(recipeRepository, productService, sourceService, logger)
		app.Controller(
			buildPath(ApiPrefix, "recipes"),
			new(controllers.RecipeController),
			recipeService,
			sourceService,
		)
		app.Controller(
			buildPath(InternalPrefix, "recipes"),
			new(intercontrollers.RecipeController),
			recipeService,
			sourceService,
		)
	}

	logger.Debug("Available routes:")
	for _, r := range app.GetRoutes() {
		logger.Debug(r.String())
	}

	// Start the server using a network address.
	app.Run(
		iris.Addr(":8080"),
		iris.WithOptimizations,
	)
}
