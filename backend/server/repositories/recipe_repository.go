package repositories

import (
	"errors"

	"context"

	"reflect"

	"strings"

	"gitlab.com/msrpw/repo/backend/server/datamodels"
	"gitlab.com/msrpw/repo/backend/server/datasource"
	"gopkg.in/olivere/elastic.v5"
)

type RecipeRepository interface {
	Insert(recipe datamodels.Recipe) (id string, ok bool, err error)
	InsertOrUpdate(recipe datamodels.Recipe) (result datamodels.Recipe, updated bool, err error)
	Select(id string) (recipe datamodels.Recipe, err error)
	SelectByName(name string) (recipe datamodels.Recipe, err error)
	SelectByUrl(url string) (recipe datamodels.Recipe, err error)
	SelectAll() (recipes []datamodels.Recipe, err error)
	SelectByProductsNames(products []string) (recipes []datamodels.Recipe, err error)
	Delete(id string) (ok bool, err error)
}

func NewRecipeRepository(client *datasource.ElasticClient) RecipeRepository {
	return &recipeRepository{client: client}
}

type recipeRepository struct {
	client *datasource.ElasticClient
}

func (r *recipeRepository) exists(recipe datamodels.Recipe) (id string, exists bool, err error) {
	nameQuery := elastic.NewTermQuery("name", recipe.Name)
	sourceIdQuery := elastic.NewTermQuery("source_id", recipe.SourceId)

	boolQuery := elastic.NewBoolQuery().Must(nameQuery, sourceIdQuery)

	existing, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(recipe.DocumentType()).
		Query(boolQuery).
		Size(1).
		Do(context.Background())
	if err != nil {
		return "", false, err
	}
	if existing.TotalHits() > 0 {
		found := existing.Hits.Hits[0]
		return found.Id, true, nil
	}
	return "", false, nil
}

func (r *recipeRepository) Insert(recipe datamodels.Recipe) (id string, ok bool, err error) {
	recipe.Name = strings.ToLower(recipe.Name)
	foundId, exists, err := r.exists(recipe)
	if err != nil {
		return "", true, err
	}
	if exists {
		return foundId, false, nil
	}
	result, err := r.client.Client.Index().
		Index(r.client.Index).
		Type(recipe.DocumentType()).
		BodyJson(recipe).
		Do(context.Background())
	if err != nil {
		return "", true, err
	}
	return result.Id, true, nil
}

func (r *recipeRepository) InsertOrUpdate(recipe datamodels.Recipe) (result datamodels.Recipe, updated bool, err error) {
	return result, updated, errors.New("not yet implemented")
}

func (r *recipeRepository) Select(id string) (recipe datamodels.Recipe, err error) {
	return recipe, errors.New("not yet implemented")
}

func (r *recipeRepository) SelectByName(name string) (recipe datamodels.Recipe, err error) {
	return recipe, errors.New("not yet implemented")
}

func (r *recipeRepository) SelectByUrl(url string) (recipe datamodels.Recipe, err error) {
	return recipe, errors.New("not yet implemented")
}

func (r *recipeRepository) SelectAll() (recipes []datamodels.Recipe, err error) {
	searchResult, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(datamodels.RecipeDocument).
		Sort("name", true).
		Do(context.Background())
	if err != nil {
		return recipes, err
	}
	var ttyp datamodels.Recipe
	for i, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		if s, ok := item.(datamodels.Recipe); ok {
			s.Id = searchResult.Hits.Hits[i].Id
			recipes = append(recipes, s)
		}
	}
	return recipes, nil
}

func (r *recipeRepository) SelectByProductsNames(products []string) (recipes []datamodels.Recipe, err error) {
	boolQuery := elastic.NewBoolQuery().MinimumNumberShouldMatch(1)
	for _, p := range products {
		boolQuery.Should(elastic.NewTermQuery("products.product.name", p))
	}
	nestedQuery := elastic.NewNestedQuery("products", boolQuery)

	searchResult, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(datamodels.RecipeDocument).
		Query(nestedQuery).
		Size(64).
		Do(context.Background())
	if err != nil {
		return nil, err
	}

	var ttyp datamodels.Recipe
	for i, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		if s, ok := item.(datamodels.Recipe); ok {
			s.Id = searchResult.Hits.Hits[i].Id
			recipes = append(recipes, s)
		}
	}

	return recipes, nil
}

func (r *recipeRepository) Delete(id string) (ok bool, err error) {
	return ok, errors.New("not yet implemented")
}
