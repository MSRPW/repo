package repositories

import (
	"context"

	"reflect"

	"encoding/json"

	"fmt"

	"strings"

	"gitlab.com/msrpw/repo/backend/server/datamodels"
	"gitlab.com/msrpw/repo/backend/server/datasource"
	"gopkg.in/olivere/elastic.v5"
)

type ProductRepository interface {
	Insert(product datamodels.Product) (id string, ok bool, err error)
	Select(id string) (product datamodels.Product, err error)
	SelectByName(name string) (product datamodels.Product, err error)
	SelectAll() (products []datamodels.Product, err error)
	Suggest(query string) (products []datamodels.Product, err error)
	Delete(id string) (ok bool, err error)
	DeleteByName(name string) (ok bool, err error)
}

func NewProductRepository(client *datasource.ElasticClient) ProductRepository {
	return &productRepository{client: client}
}

type productRepository struct {
	client *datasource.ElasticClient
}

func (r *productRepository) exists(product datamodels.Product) (id string, exists bool, err error) {
	termQuery := elastic.NewTermQuery("name", product.Name)
	existing, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(product.DocumentType()).
		Query(termQuery).
		Size(1).
		Do(context.Background())
	if err != nil {
		return "", false, err
	}
	if existing.TotalHits() > 0 {
		found := existing.Hits.Hits[0]
		return found.Id, true, nil
	}
	return "", false, nil
}

func (r *productRepository) Insert(product datamodels.Product) (id string, ok bool, err error) {
	product.Name = strings.ToLower(product.Name)
	suggestions := append(strings.Split(product.Name, " "), product.Name)
	product.Suggest = elastic.NewSuggestField(suggestions...)
	foundId, exists, err := r.exists(product)
	if err != nil {
		return "", true, err
	}
	if exists {
		return foundId, !exists, nil
	}
	result, err := r.client.Client.Index().
		Index(r.client.Index).
		Type(product.DocumentType()).
		BodyJson(product).
		Do(context.Background())
	if err != nil {
		return "", true, err
	}

	return result.Id, true, nil
}

func (r *productRepository) Select(id string) (product datamodels.Product, err error) {
	get, err := r.client.Client.Get().
		Index(r.client.Index).
		Type(product.DocumentType()).
		Id(id).
		Do(context.Background())
	if err != nil {
		return product, err
	}
	err = json.Unmarshal(*get.Source, &product)
	if err != nil {
		return product, err
	}
	product.Id = get.Id
	return product, nil
}

func (r *productRepository) SelectByName(name string) (product datamodels.Product, err error) {
	termQuery := elastic.NewTermQuery("name", name)
	existing, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(product.DocumentType()).
		Query(termQuery).
		Size(1).
		Do(context.Background())
	if err != nil {
		return product, err
	}
	if existing.TotalHits() > 0 {
		found := existing.Hits.Hits[0]
		err = json.Unmarshal(*found.Source, &product)
		if err != nil {
			return product, err
		}
		product.Id = found.Id
		return product, nil
	}
	return product, fmt.Errorf("product with name %s does not exist", name)
}

func (r *productRepository) SelectAll() (products []datamodels.Product, err error) {
	searchResult, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(datamodels.ProductDocument).
		Sort("name", true).
		Do(context.Background())
	if err != nil {
		return products, err
	}
	var ttyp datamodels.Product
	for i, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		if p, ok := item.(datamodels.Product); ok {
			p.Id = searchResult.Hits.Hits[i].Id
			products = append(products, p)
		}
	}
	return products, nil
}

func (r *productRepository) Suggest(query string) (products []datamodels.Product, err error) {
	suggesterName := "product-completion"
	cs := elastic.NewFuzzyCompletionSuggester(suggesterName)
	cs = cs.Text(query)
	cs = cs.Field("suggest_field")
	searchResult, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(datamodels.ProductDocument).
		Sort("name", true).
		Suggester(cs).
		Do(context.Background())
	if err != nil {
		return products, err
	}
	suggestions, found := searchResult.Suggest[suggesterName]
	if !found {
		return products, nil
	}
	suggestion := suggestions[0]
	for _, item := range suggestion.Options {
		p, err := r.Select(item.Id)
		if err != nil {
			return products, err
		}
		products = append(products, p)
	}
	return products, nil
}

func (r *productRepository) Delete(id string) (ok bool, err error) {
	res, err := r.client.Client.Delete().
		Index(r.client.Index).
		Type(datamodels.ProductDocument).
		Id(id).
		Do(context.Background())
	ok = res.Found
	if err != nil {
		return ok, err
	}
	return ok, nil
}

func (r *productRepository) DeleteByName(name string) (ok bool, err error) {
	q := elastic.NewTermQuery("name", name)
	res, err := r.client.Client.DeleteByQuery().
		Index(r.client.Index).
		Type(datamodels.ProductDocument).
		Query(q).
		Do(context.Background())
	ok = res.Deleted == 1
	if err != nil {
		return ok, err
	}
	return ok, nil
}
