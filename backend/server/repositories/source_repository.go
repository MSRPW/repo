package repositories

import (
	"errors"

	"context"
	"reflect"

	"encoding/json"

	"fmt"

	"strings"

	"gitlab.com/msrpw/repo/backend/server/datamodels"
	"gitlab.com/msrpw/repo/backend/server/datasource"
	"gopkg.in/olivere/elastic.v5"
)

type SourceRepository interface {
	Insert(source datamodels.Source) (id string, ok bool, err error)
	InsertOrUpdate(source datamodels.Source) (result datamodels.Source, updated bool, err error)
	Select(id string) (source datamodels.Source, err error)
	SelectByName(name string) (source datamodels.Source, err error)
	SelectByUrl(url string) (source datamodels.Source, err error)
	SelectAll() (sources []datamodels.Source, err error)
	Delete(id string) (ok bool, err error)
	DeleteByName(name string) (ok bool, err error)
}

func NewSourceRepository(client *datasource.ElasticClient) SourceRepository {
	return &sourceRepository{client: client}
}

type sourceRepository struct {
	client *datasource.ElasticClient
}

func (r *sourceRepository) exists(source datamodels.Source) (id string, exists bool, err error) {
	termQuery := elastic.NewTermQuery("name", source.Name)
	existing, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(source.DocumentType()).
		Query(termQuery).
		Size(1).
		Do(context.Background())
	if err != nil {
		return "", false, err
	}
	if existing.TotalHits() > 0 {
		found := existing.Hits.Hits[0]
		return found.Id, true, nil
	}
	return "", false, nil
}

func (r *sourceRepository) Insert(source datamodels.Source) (id string, ok bool, err error) {
	source.Name = strings.ToLower(source.Name)
	foundId, exists, err := r.exists(source)
	if err != nil {
		return "", true, err
	}
	if exists {
		return foundId, false, nil
	}
	result, err := r.client.Client.Index().
		Index(r.client.Index).
		Type(source.DocumentType()).
		BodyJson(source).
		Do(context.Background())
	if err != nil {
		return "", true, err
	}
	return result.Id, true, nil
}

func (r *sourceRepository) InsertOrUpdate(source datamodels.Source) (result datamodels.Source, updated bool, err error) {
	return result, updated, errors.New("not yet implemented")
}

func (r *sourceRepository) Select(id string) (source datamodels.Source, err error) {
	get, err := r.client.Client.Get().
		Index(r.client.Index).
		Type(source.DocumentType()).
		Id(id).
		Do(context.Background())
	if err != nil {
		return source, err
	}
	err = json.Unmarshal(*get.Source, &source)
	if err != nil {
		return source, err
	}
	source.Id = get.Id
	return source, nil
}

func (r *sourceRepository) SelectByName(name string) (source datamodels.Source, err error) {
	termQuery := elastic.NewTermQuery("name", name)
	existing, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(source.DocumentType()).
		Query(termQuery).
		Size(1).
		Do(context.Background())
	if err != nil {
		return source, err
	}
	if existing.TotalHits() > 0 {
		found := existing.Hits.Hits[0]
		err = json.Unmarshal(*found.Source, &source)
		if err != nil {
			return source, err
		}
		source.Id = found.Id
		return source, nil
	}
	return source, fmt.Errorf("source with name %s does not exist", name)
}

func (r *sourceRepository) SelectByUrl(url string) (source datamodels.Source, err error) {
	return source, errors.New("not yet implemented")
}

func (r *sourceRepository) SelectAll() (sources []datamodels.Source, err error) {
	searchResult, err := r.client.Client.Search().
		Index(r.client.Index).
		Type(datamodels.SourceDocument).
		Sort("name", true).
		Do(context.Background())
	if err != nil {
		return sources, err
	}
	var ttyp datamodels.Source
	for i, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		if s, ok := item.(datamodels.Source); ok {
			s.Id = searchResult.Hits.Hits[i].Id
			sources = append(sources, s)
		}
	}
	return sources, nil
}

func (r *sourceRepository) Delete(id string) (ok bool, err error) {
	return ok, errors.New("not yet implemented")
}

func (r *sourceRepository) DeleteByName(name string) (ok bool, err error) {
	return ok, errors.New("not yet implemented")
}
