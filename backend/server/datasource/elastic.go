package datasource

import (
	"context"

	"net"
	"time"

	"gitlab.com/msrpw/repo/backend/server/logging"
	es "gopkg.in/olivere/elastic.v5"
)

type ElasticClient struct {
	Client  *es.Client
	Index   string
	mapping string
}

func NewClient(
	ctx context.Context,
	logger *logging.Logger,
	index string,
	mapping string,
) (client *ElasticClient, err error) {
	conn, err := net.Dial("tcp", "elasticsearch:9200")
	for err != nil {
		logger.Info("Elasticsearch not available, trying again in 5s...")
		time.Sleep(5 * time.Second)
		conn, err = net.Dial("tcp", "elasticsearch:9200")
	}
	conn.Close()

	c, err := es.NewClient(
		es.SetURL("http://elasticsearch:9200"),
		es.SetBasicAuth("elastic", "changeme"),
	)
	if err != nil {
		return nil, err
	}

	client = &ElasticClient{c, index, mapping}

	esversion, err := c.ElasticsearchVersion("http://elasticsearch:9200")
	if err != nil {
		return client, err
	}
	logger.Infof("Elasticsearch version %s\n", esversion)

	exists, err := c.IndexExists(index).Do(ctx)
	if err != nil {
		return client, err
	}
	if !exists {
		createIndex, err := c.CreateIndex(index).BodyString(mapping).Do(ctx)
		if err != nil {
			return client, err
		}
		if !createIndex.Acknowledged {
			logger.Warningf("Elasticsearch index %s creation not acknowledged\n", index)
		} else {
			logger.Infof("Index %s created in Elasticsearch\n", index)
		}
	} else {
		logger.Infof("Index %s exists in Elasticsearch, skipping creation\n", index)
	}

	_, err = c.Flush().Index(index).Do(ctx)
	if err != nil {
		return client, err
	}

	return client, nil
}

const RecipeFinderMapping = `
{
	"settings": {
		"number_of_shards": 1,
		"number_of_replicas": 0
	},
	"mappings": {
		"%s": {
			"properties": {
				"name": {
					"type": "keyword"
				},
				"url": {
					"type": "text",
					"store": true,
					"fielddata": true
				}
			}
		},
		"product":{
			"properties": {
				"name": {
					"type": "keyword"
				},
				"suggest_field": {
					"type": "completion"
				}
			}
		},
		"recipe":{
			"properties": {
				"source_id": {
					"type": "keyword"
				},
				"name": {
					"type": "keyword"
				},
				"url": {
					"type": "text",
					"store": true,
					"fielddata": true
				},
				"products": {
					"type": "nested",
					"properties": {
						"product": {
							"properties": {
								"name": {
									"type": "keyword"
								}
							}
						},
						"value": {
							"type": "text"
						}
					}
				},
				"parsed": {
					"type": "date"
				},
				"photoUrl": {
					"type": "text",
					"store": true,
					"fielddata": true
				}
			}
		}
	}
}
`
