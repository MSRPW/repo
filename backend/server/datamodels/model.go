package datamodels

type Model interface {
	DocumentType() string
}
