package datamodels

import "gopkg.in/olivere/elastic.v5"

const ProductDocument = "product"

// Product is a product that was found in any recipe
type Product struct {
	Id      string                `json:"id"`
	Name    string                `json:"name"`
	Suggest *elastic.SuggestField `json:"suggest_field,omitempty"`
}

func (p *Product) DocumentType() string { return ProductDocument }
