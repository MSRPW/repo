package datamodels

import "time"

const RecipeDocument = "recipe"

// Recipe is a parsed recipe from one of the sources
type Recipe struct {
	Id       string          `json:"id"`
	SourceId string          `json:"source_id"`
	Name     string          `json:"name"`
	Url      string          `json:"url"`
	Products []RecipeProduct `json:"products"`
	Parsed   time.Time       `json:"parsed,omitempty"`
	PhotoUrl string          `json:"photoUrl,omitempty"`
}

type RecipeProduct struct {
	Product Product `json:"product"`
	Value   string  `json:"value"`
}

func (r *Recipe) DocumentType() string { return RecipeDocument }
