package datamodels

const SourceDocument = "source"

// Source is a website/blog reference from which recipes are parsed
type Source struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Url  string `json:"url"`
}

func (s *Source) DocumentType() string { return SourceDocument }
