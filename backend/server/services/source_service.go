package services

import (
	"errors"

	"gitlab.com/msrpw/repo/backend/server/datamodels"
	"gitlab.com/msrpw/repo/backend/server/logging"
	"gitlab.com/msrpw/repo/backend/server/repositories"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type SourceService interface {
	Put(source viewmodels.Source) (id string, ok bool)
	UpdateSource(source viewmodels.Source) (ok bool)
	GetById(id string) (source datamodels.Source, ok bool)
	GetByName(name string) (source datamodels.Source, ok bool)
	GetByUrl(url string) (source datamodels.Source, ok bool)
	GetAll() (sources []datamodels.Source)
	DeleteById(id string) (ok bool)
	DeleteByName(name string) (ok bool)
}

func NewSourceService(repo repositories.SourceRepository, logger *logging.Logger) SourceService {
	return &sourceService{
		repo:   repo,
		logger: logger,
	}
}

type sourceService struct {
	repo   repositories.SourceRepository
	logger *logging.Logger
}

func (s *sourceService) Put(source viewmodels.Source) (id string, ok bool) {
	ss := datamodels.Source{
		Name: source.Name,
		Url:  source.Url,
	}

	id, ok, err := s.repo.Insert(ss)
	if err != nil {
		s.logger.Error(err.Error())
	} else if !ok {
		s.logger.Warningf("Source with name `%s` already exists under index `%s`", source.Name, id)
	} else {
		s.logger.Infof("Indexed source `%s` under index `%s`\n", source.Name, id)
	}
	return
}

func (s *sourceService) UpdateSource(source viewmodels.Source) (ok bool) {
	panic(errors.New("not yet implemented"))
	return
}

func (s *sourceService) GetById(id string) (source datamodels.Source, ok bool) {
	source, err := s.repo.Select(id)
	if err != nil {
		s.logger.Error(err.Error())
		return source, false
	}
	return source, true
}

func (s *sourceService) GetByName(name string) (source datamodels.Source, ok bool) {
	source, err := s.repo.SelectByName(name)
	if err != nil {
		s.logger.Error(err.Error())
		return source, false
	}
	return source, true
}

func (s *sourceService) GetByUrl(url string) (source datamodels.Source, ok bool) {
	panic(errors.New("not yet implemented"))
	return
}

func (s *sourceService) GetAll() (sources []datamodels.Source) {
	sources, err := s.repo.SelectAll()
	if err != nil {
		s.logger.Error(err.Error())
	}
	return
}

func (s *sourceService) DeleteById(id string) (ok bool) {
	panic(errors.New("not yet implemented"))
	return
}

func (s *sourceService) DeleteByName(name string) (ok bool) {
	panic(errors.New("not yet implemented"))
	return
}
