package services

import (
	"fmt"

	"gitlab.com/msrpw/repo/backend/server/datamodels"
	"gitlab.com/msrpw/repo/backend/server/logging"
	"gitlab.com/msrpw/repo/backend/server/repositories"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type ProductService interface {
	Put(product viewmodels.Product) (id string, ok bool)
	GetById(id string) (product datamodels.Product, ok bool)
	GetByName(name string) (product datamodels.Product, ok bool)
	GetAll() (products []datamodels.Product)
	GetSuggestions(query string) (products []datamodels.Product)
	DeleteById(id string) (ok bool)
	DeleteByName(name string) (ok bool)
}

func NewProductService(repo repositories.ProductRepository, logger *logging.Logger) ProductService {
	return &productService{
		repo:   repo,
		logger: logger,
	}
}

type productService struct {
	repo   repositories.ProductRepository
	logger *logging.Logger
}

func (s *productService) Put(product viewmodels.Product) (id string, ok bool) {
	p := datamodels.Product{
		Name: product.Name,
	}
	id, ok, err := s.repo.Insert(p)
	if err != nil {
		s.logger.Error(err.Error())
	} else if !ok {
		s.logger.Warningf("Product with name `%s` already exists under index `%s`", product.Name, id)
	} else {
		s.logger.Infof("Indexed product `%s` under index `%s`\n", product.Name, id)
	}
	return
}

func (s *productService) GetById(id string) (product datamodels.Product, ok bool) {
	product, err := s.repo.Select(id)
	if err != nil {
		s.logger.Error(err.Error())
		return product, false
	}
	return product, true
}

func (s *productService) GetByName(name string) (product datamodels.Product, ok bool) {
	product, err := s.repo.SelectByName(name)
	if err != nil {
		s.logger.Error(err.Error())
		return product, false
	}
	return product, true
}

func (s *productService) GetAll() (products []datamodels.Product) {
	products, err := s.repo.SelectAll()
	if err != nil {
		s.logger.Error(err.Error())
	}
	return
}

func (s *productService) GetSuggestions(query string) (products []datamodels.Product) {
	products, err := s.repo.Suggest(query)
	s.logger.Debug(fmt.Sprintf("requested suggestions for: `%s`", query))
	if err != nil {
		s.logger.Error(err.Error())
	}
	return
}

func (s *productService) DeleteById(id string) (ok bool) {
	ok, err := s.repo.Delete(id)
	if err != nil {
		s.logger.Error(err.Error())
	}
	return
}

func (s *productService) DeleteByName(name string) (ok bool) {
	ok, err := s.repo.DeleteByName(name)
	if err != nil {
		s.logger.Error(err.Error())
	}
	return
}
