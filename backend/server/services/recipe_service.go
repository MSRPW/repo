package services

import (
	"errors"

	"time"

	"gitlab.com/msrpw/repo/backend/server/datamodels"
	"gitlab.com/msrpw/repo/backend/server/logging"
	"gitlab.com/msrpw/repo/backend/server/repositories"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type RecipeService interface {
	Put(recipe viewmodels.Recipe) (id string, ok bool)
	UpdateRecipe(recipe viewmodels.Recipe) (ok bool)
	GetById(id string) (recipe datamodels.Recipe, ok bool)
	GetByName(name string) (recipe datamodels.Recipe, ok bool)
	GetByUrl(url string) (recipe datamodels.Recipe, ok bool)
	GetAll() (recipes []datamodels.Recipe)
	GetByProductsNames(products []string) (recipes []datamodels.Recipe)
	DeleteById(id string) (ok bool)
}

func NewRecipeService(
	repo repositories.RecipeRepository,
	pService ProductService,
	sService SourceService,
	logger *logging.Logger,
) RecipeService {
	return &recipeService{
		repo:           repo,
		productService: pService,
		sourceService:  sService,
		logger:         logger,
	}
}

type recipeService struct {
	repo           repositories.RecipeRepository
	productService ProductService
	sourceService  SourceService
	logger         *logging.Logger
}

func (s *recipeService) Put(recipe viewmodels.Recipe) (id string, ok bool) {
	var source datamodels.Source
	{
		if src, ok := s.sourceService.GetById(recipe.SourceId); !ok {
			s.logger.Errorf("Recipe's source could not be established, "+
				"source with id `%s` was not found in the database\n", recipe.SourceId)
			return "", ok
		} else {
			source = src
		}
	}

	var products []datamodels.RecipeProduct
	{
		for _, vmP := range recipe.Products {
			id, ok = s.productService.Put(vmP)
			if !ok && id == "" {
				s.logger.Errorf("Error creating product with name `%s`\n", vmP.Name)
				return
			}
			dmP, ok := s.productService.GetById(id)
			if !ok {
				s.logger.Errorf("Error processing product with name `%s`\n", vmP.Name)
			}
			dmRP := datamodels.RecipeProduct{
				Product: dmP,
				Value:   vmP.Value,
			}
			products = append(products, dmRP)
		}
	}

	r := datamodels.Recipe{
		SourceId: source.Id,
		Name:     recipe.Name,
		Url:      recipe.Url,
		Products: products,
		Parsed:   time.Now(),
		PhotoUrl: recipe.PhotoUrl,
	}

	id, ok, err := s.repo.Insert(r)
	if err != nil {
		s.logger.Error(err.Error())
	} else if !ok {
		s.logger.Warningf("Recipe with name `%s` already exists under index `%s`\n", recipe.Name, id)
	} else {
		s.logger.Infof("Indexed recipe `%s` under index `%s`\n", recipe.Name, id)
	}
	return
}

func (s *recipeService) UpdateRecipe(recipe viewmodels.Recipe) (ok bool) {
	panic(errors.New("not yet implemented"))
	return
}

func (s *recipeService) GetById(id string) (recipe datamodels.Recipe, ok bool) {
	panic(errors.New("not yet implemented"))
	return
}

func (s *recipeService) GetByName(name string) (recipe datamodels.Recipe, ok bool) {
	panic(errors.New("not yet implemented"))
	return
}

func (s *recipeService) GetByUrl(url string) (recipe datamodels.Recipe, ok bool) {
	panic(errors.New("not yet implemented"))
	return
}

func (s *recipeService) GetAll() (recipes []datamodels.Recipe) {
	recipes, err := s.repo.SelectAll()
	if err != nil {
		s.logger.Error(err.Error())
	}
	return
}

func (s *recipeService) GetByProductsNames(products []string) (recipes []datamodels.Recipe) {
	recipes, err := s.repo.SelectByProductsNames(products)
	if err != nil {
		s.logger.Error(err.Error())
	}
	return
}

func (s *recipeService) DeleteById(id string) (ok bool) {
	panic(errors.New("not yet implemented"))
	return
}
