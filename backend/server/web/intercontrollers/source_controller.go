package intercontrollers

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	"gitlab.com/msrpw/repo/backend/server/services"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type SourceController struct {
	mvc.C
	Service services.SourceService
}

// GetAll returns list of the sources.
// /sources/all
func (c *SourceController) GetAll() (sources viewmodels.SourcesList) {
	data := c.Service.GetAll()

	for _, s := range data {
		sources.Sources = append(sources.Sources, viewmodels.Source{Id: s.Id, Name: s.Name, Url: s.Url})
	}
	sources.Length = len(data)
	return
}

// Post inserts a source.
// /sources
func (c *SourceController) Post() (interface{}, int) {
	source := &viewmodels.Source{}
	if err := c.Ctx.ReadJSON(source); err != nil {
		return iris.Map{"error": err.Error()}, iris.StatusBadRequest
	}

	if source.Name == "" || source.Url == "" {
		return iris.Map{"error": "Provide all required fields: name, url"}, iris.StatusBadRequest
	}

	id, created := c.Service.Put(*source)
	source.Id = id

	if !created {
		return source, iris.StatusConflict
	}

	return source, iris.StatusCreated
}
