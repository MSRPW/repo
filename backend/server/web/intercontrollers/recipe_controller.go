package intercontrollers

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	"gitlab.com/msrpw/repo/backend/server/services"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type RecipeController struct {
	mvc.C
	Service       services.RecipeService
	SourceService services.SourceService
}

// GetAll returns list of the recipes.
// /sources/all
func (c *RecipeController) GetAll() (recipes viewmodels.RecipesList) {
	data := c.Service.GetAll()

	for _, r := range data {
		var products []viewmodels.Product
		{
			for _, p := range r.Products {
				products = append(products, viewmodels.Product{
					Id:    p.Product.Id,
					Name:  p.Product.Name,
					Value: p.Value,
				})
			}
		}
		recipes.Recipes = append(recipes.Recipes, viewmodels.Recipe{
			Id:       r.Id,
			SourceId: r.SourceId,
			Name:     r.Name,
			Url:      r.Url,
			Products: products,
			PhotoUrl: r.PhotoUrl,
		})
	}
	recipes.Length = len(data)
	return
}

// Post inserts a recipe.
// /recipes
func (c *RecipeController) Post() (interface{}, int) {
	recipe := &viewmodels.Recipe{}
	if err := c.Ctx.ReadJSON(&recipe); err != nil {
		return iris.Map{"error": err.Error()}, iris.StatusBadRequest
	}

	if recipe.Name == "" || recipe.Url == "" || recipe.SourceId == "" || len(recipe.Products) == 0 {
		return iris.Map{"error": "Provide all required fields: name, url, sourceId, products"}, iris.StatusBadRequest
	}

	id, created := c.Service.Put(*recipe)
	recipe.Id = id

	if !created {
		if id == "" {
			return iris.Map{"error": "Source with given index not found"}, iris.StatusBadRequest
		} else {
			return recipe, iris.StatusConflict
		}
	}

	return recipe, iris.StatusCreated
}
