package intercontrollers

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	"gitlab.com/msrpw/repo/backend/server/services"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type ProductController struct {
	mvc.C
	Service services.ProductService
}

// GetAll returns list of the products.
// /products/all
func (c *ProductController) GetAll() (products viewmodels.ProductsList) {
	data := c.Service.GetAll()

	for _, p := range data {
		products.Products = append(products.Products, viewmodels.Product{Id: p.Id, Name: p.Name})
	}
	products.Length = len(data)

	return
}

// GetIdBy returns product by id.
// /products/id/{id:string}
func (c *ProductController) GetIdBy(id string) (product viewmodels.Product, found bool) {
	data, found := c.Service.GetById(id)
	if !found {
		return viewmodels.Product{}, found
	}
	product = viewmodels.Product{Id: data.Id, Name: data.Name}
	return product, found
}

// GetIdByName returns product's name by id.
// /products/id/{id:string}/name
func (c *ProductController) GetIdByName(id string) (name string, found bool) {
	data, found := c.Service.GetById(id)
	if !found {
		return "", found
	}
	name = data.Name
	return name, found
}

// GetNameBy returns product by name.
// /products/name/{name:string}
func (c *ProductController) GetNameBy(name string) (product viewmodels.Product, found bool) {
	data, found := c.Service.GetByName(name)
	if !found {
		return viewmodels.Product{}, found
	}
	product = viewmodels.Product{Id: data.Id, Name: data.Name}
	return product, found
}

// GetNameById returns product's id by name.
// /products/name/{name:string}/id
func (c *ProductController) GetNameById(name string) (id string, found bool) {
	data, found := c.Service.GetByName(name)
	if !found {
		return "", found
	}
	id = data.Id
	return id, found
}

// Post inserts product.
// /products
func (c *ProductController) Post() (interface{}, int) {
	product := &viewmodels.Product{}
	if err := c.Ctx.ReadJSON(product); err != nil {
		return iris.Map{"error": err.Error()}, iris.StatusBadRequest
	}

	if product.Name == "" {
		return iris.Map{"error": "Provide all required fields: name"}, iris.StatusBadRequest
	}

	id, created := c.Service.Put(*product)
	product.Id = id

	if !created {
		return product, iris.StatusConflict
	}

	return product, iris.StatusCreated
}

// PostSaveName inserts a product by name.
// /products/name
func (c *ProductController) PostName() (interface{}, int) {
	var name string
	if err := c.Ctx.ReadJSON(&name); err != nil {
		return iris.Map{"error": err.Error()}, iris.StatusBadRequest
	}

	if name == "" {
		return iris.Map{"error": "Provide proper name string"}, iris.StatusBadRequest
	}

	product := viewmodels.Product{Name: name}

	id, created := c.Service.Put(product)
	product.Id = id

	if !created {
		return product, iris.StatusConflict
	}

	return product, iris.StatusCreated
}

// DeleteIdBy deletes a product by id.
// /products/id/{id:string}
func (c *ProductController) DeleteIdBy(id string) interface{} {
	wasDel := c.Service.DeleteById(id)
	if wasDel {
		return iris.Map{"deleted": id}
	}
	return iris.StatusNotFound
}

// DeleteDeleteNameBy deletes a product by id.
// /products/name/{name:string}
func (c *ProductController) DeleteNameBy(name string) interface{} {
	wasDel := c.Service.DeleteByName(name)
	if wasDel {
		return iris.Map{"deleted": name}
	}
	return iris.StatusNotFound
}
