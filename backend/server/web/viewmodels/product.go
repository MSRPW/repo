package viewmodels

type Product struct {
	Id    string `json:"id,omitempty"`
	Name  string `json:"name"`
	Value string `json:"value,omitempty"`
}

type ProductsList struct {
	Products []Product `json:"products"`
	Length   int       `json:"length"`
}

type ProductsNameList struct {
	Products []string `json:"products"`
	Length   int      `json:"length"`
}
