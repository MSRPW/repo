package viewmodels

type Source struct {
	Id   string `json:"id,omitempty"`
	Name string `json:"name"`
	Url  string `json:"url"`
}

type SourcesList struct {
	Sources []Source `json:"sources"`
	Length  int      `json:"length"`
}
