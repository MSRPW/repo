package apicontrollers

import (
	"github.com/kataras/iris/mvc"
	"gitlab.com/msrpw/repo/backend/server/services"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type ProductController struct {
	mvc.C
	Service services.ProductService
}

// GetAll returns list of the products.
// /products/all
func (c *ProductController) GetAll() (products viewmodels.ProductsList) {
	data := c.Service.GetAll()

	for _, product := range data {
		products.Products = append(products.Products, viewmodels.Product{Name: product.Name})
	}
	products.Length = len(data)

	return
}

// GetAllNames returns list of the products names.
// /products/all/names
func (c *ProductController) GetAllNames() (products viewmodels.ProductsNameList) {
	data := c.Service.GetAll()

	for _, product := range data {
		products.Products = append(products.Products, product.Name)
	}
	products.Length = len(data)

	return
}

// GetSuggest returns list of suggested products for given query.
// /products/suggest/{query: string}
func (c *ProductController) GetSuggestBy(query string) (products viewmodels.ProductsNameList) {
	data := c.Service.GetSuggestions(query)

	for _, product := range data {
		products.Products = append(products.Products, product.Name)
	}
	products.Length = len(data)

	return
}
