package apicontrollers

import (
	"github.com/kataras/iris/mvc"
	"gitlab.com/msrpw/repo/backend/server/services"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type SourceController struct {
	mvc.C
	Service services.SourceService
}

// GetAll returns list of the sources.
// /sources/all
func (c *SourceController) GetAll() (sources viewmodels.SourcesList) {
	data := c.Service.GetAll()

	for _, s := range data {
		sources.Sources = append(sources.Sources, viewmodels.Source{Name: s.Name, Url: s.Url})
	}
	sources.Length = len(data)
	return
}
