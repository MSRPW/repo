package apicontrollers

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	"gitlab.com/msrpw/repo/backend/server/services"
	"gitlab.com/msrpw/repo/backend/server/web/viewmodels"
)

type RecipeController struct {
	mvc.C
	Service       services.RecipeService
	SourceService services.SourceService
}

// PostSearch returns list of the recipes filtered with provided products.
// /recipes/search
func (c *RecipeController) PostSearch() (interface{}, int) {
	productsList := &viewmodels.ProductsNameList{}
	if err := c.Ctx.ReadJSON(productsList); err != nil {
		return iris.Map{"error": err.Error()}, iris.StatusBadRequest
	}

	if len(productsList.Products) == 0 {
		return iris.Map{"error": "Products list is empty"}, iris.StatusBadRequest
	}

	data := c.Service.GetByProductsNames(productsList.Products)

	recipes := &viewmodels.RecipesList{}
	for _, r := range data {
		var products []viewmodels.Product
		{
			for _, p := range r.Products {
				products = append(products, viewmodels.Product{
					Id:    p.Product.Id,
					Name:  p.Product.Name,
					Value: p.Value,
				})
			}
		}
		recipe := viewmodels.Recipe{
			Name:     r.Name,
			Url:      r.Url,
			Products: products,
			PhotoUrl: r.PhotoUrl,
		}
		source, ok := c.SourceService.GetById(r.SourceId)
		if ok {
			recipe.Source = viewmodels.Source{
				Name: source.Name,
				Url:  source.Url,
			}
		}
		recipes.Recipes = append(recipes.Recipes, recipe)
	}
	recipes.Length = len(data)
	return recipes, iris.StatusOK
}
