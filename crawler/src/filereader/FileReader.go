package filereader

import (
	"bufio"
	"fmt"
	"os"
)

type AbstractFileReader interface {
	ReadProductsFromFile() []string
}

type FileReader struct {
	FileName string
}

func (fr FileReader) ReadProductsFromFile() ([]string, error) {

	var products = make([]string, 0)
	file, err := os.Open(fr.FileName)

	if err != nil {
		fmt.Println("Couldn't load products file")
		return nil, err
	}

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		products = append(products, scanner.Text())
	}

	return products, nil
}
