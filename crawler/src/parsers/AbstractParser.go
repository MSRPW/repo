package parsers

import models "gitlab.com/msrpw/repo/crawler/src/viewmodels"

type AbstractParser interface {
	ParseWebsite(url string, sourceId string) (models.Recipe, error)
}
