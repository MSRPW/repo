package parsers

import (
	models "gitlab.com/msrpw/repo/crawler/src/viewmodels"

	"log"
	"net/http"
	"strings"

	"errors"

	"github.com/PuerkitoBio/goquery"
)

type AllrecipesParser struct {
	Products []string
}

func (parser AllrecipesParser) ParseWebsite(url string, sourceId string) (models.Recipe, error) {

	res, errGet := http.Get(url)
	doc, errDoc := goquery.NewDocumentFromResponse(res)

	if errGet != nil || errDoc != nil || res.StatusCode != 200 {
		log.Println("Error occured while coneccting to [" + url + "]")
		return models.Recipe{}, errors.New("Error occured while coneccting to [" + url + "]")
	}

	products := parser.extractList(doc)
	name := doc.Find(".recipe-summary h1").Text()
	photoUrl, _ := doc.Find(".rec-photo").Attr("src")

	return models.Recipe{
		SourceId: sourceId,
		Name:     name,
		Url:      url,
		Products: products,
		PhotoUrl: photoUrl,
	}, nil
}

func (parser AllrecipesParser) extractList(doc *goquery.Document) []models.Product {
	products := make([]models.Product, 0)

	doc.Find("body ul li label .recipe-ingred_txt").Each(func(i int, s *goquery.Selection) {
		productName := parser.matchProduct(s.Text())
		if productName != "" {
			products = append(products, models.Product{Name: productName, Value: s.Text()})
		}
	})

	return products
}

func (parser AllrecipesParser) matchProduct(toMatch string) string {
	toMatch = strings.ToLower(toMatch)
	maxSize := 0
	var result string

	for _, product := range parser.Products {
		if strings.Contains(toMatch, product) && len(product) > maxSize {
			maxSize = len(product)
			result = product
		}
	}

	return result
}
