package parsers

import (
	models "gitlab.com/msrpw/repo/crawler/src/viewmodels"
	"log"
	"fmt"
	"net/http"
	"strings"
	"errors"
	"github.com/PuerkitoBio/goquery"
)

type FoodnetworkParser struct {
	Products []string
}

func (parser FoodnetworkParser) ParseWebsite(url string, sourceId string) (models.Recipe, error) {

	res, errGet := http.Get(url)
	doc, errDoc := goquery.NewDocumentFromResponse(res)

	if errGet != nil || errDoc != nil || res.StatusCode != 200 {
		log.Println("Error occured while coneccting to [" + url + "]")
		return models.Recipe{}, errors.New("Error occured while coneccting to [" + url + "]")
	}

	products := parser.extractList(doc)
	name := doc.Find(".o-AssetTitle__a-Headline .o-AssetTitle__a-HeadlineText").Text()

	return models.Recipe{
		SourceId: sourceId,
		Name:     name,
		Url:      url,
		Products: products,
		PhotoUrl: "",
	}, nil
}

func (parser FoodnetworkParser) extractList(doc *goquery.Document) []models.Product {
	products := make([]models.Product, 0)

	doc.Find(".o-Ingredients__a-ListItemText").Each(func(i int, s *goquery.Selection) {
		productName := parser.matchProduct(s.Text())
		if productName != "" {
			products = append(products, models.Product{Name: productName, Value: s.Text()})
		}
	})

	return products
}

func (parser FoodnetworkParser) matchProduct(toMatch string) string {
	toMatch = strings.ToLower(toMatch)
	maxSize := 0
	var result string

	for _, product := range parser.Products {
		if strings.Contains(toMatch, product) && len(product) > maxSize {
			maxSize = len(product)
			result = product
		}
	}

	return result
}
