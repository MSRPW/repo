package helpers

import "fmt"

func HandleError(err error) bool {
	if err != nil {
		fmt.Printf("error: %s\n", err)
		return true
	}
	return false
}
