package viewmodels

type Recipe struct {
	Id       string    `json:"id,omitempty"`
	SourceId string    `json:"sourceId,omitempty"`
	Source   Source    `json:"source,omitempty"`
	Name     string    `json:"name"`
	Url      string    `json:"url"`
	Products []Product `json:"products"`
	PhotoUrl string    `json:"photoUrl,omitempty"`
}

type RecipesList struct {
	Recipes []Recipe `json:"recipes"`
	Length  int      `json:"length"`
}
