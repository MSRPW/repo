package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	"gitlab.com/msrpw/repo/crawler/src/crawlers"
	"gitlab.com/msrpw/repo/crawler/src/filereader"
	"gitlab.com/msrpw/repo/crawler/src/viewmodels"

	"github.com/dghubble/sling"
)

func dialBackend() {
	_, err := net.Dial("tcp", "backend:8080")
	for err != nil {
		fmt.Println("Backend not available, trying again in 5s...")
		time.Sleep(5 * time.Second)
		_, err = net.Dial("tcp", "backend:8080")
	}
}

func main() {
	dialBackend()
	httpClient := &http.Client{}
	backendBase := sling.New().Base("http://backend:8080").Client(httpClient)

	sourcesList := new(viewmodels.SourcesList)
	_, err := backendBase.New().Get("/_internal/sources/all").ReceiveSuccess(sourcesList)

	if err != nil {
		fmt.Println("Couldn't get sources: ", err)
		return
	}

	fmt.Println("Sources:", sourcesList)

	var file = "/opt/all_ingredients.txt"
	reader := filereader.FileReader{file}
	products, err := reader.ReadProductsFromFile()

	if err != nil {
		return
	}

	fmt.Println("Products:", len(products))

	c := crawlers.New()

	c.SetRecipeHandler(func(recipe viewmodels.Recipe) {
		newRecipe := new(viewmodels.Recipe)

		fmt.Println("Sending Recipe: ", recipe)

		resp, err := backendBase.New().Post("/_internal/recipes").BodyJSON(recipe).ReceiveSuccess(newRecipe)

		if err != nil {
			fmt.Println("Couldn't add recipe: ", err, resp)
			return
		}

		if newRecipe.Id == "" {
			fmt.Println("Recipe has invalid ID")
			fmt.Println("Response", resp)
			defer resp.Body.Close()
			bodyBytes, _ := ioutil.ReadAll(resp.Body)
			bodyString := string(bodyBytes)

			fmt.Println("Content:", bodyString)
			return
		}

		fmt.Println("Added Recipe: ", newRecipe)
	})

	c.AddCrawler("http://allrecipes.com/recipes/", crawlers.NewAllrecipesCrawler(products))
	c.AddCrawler("http://www.foodnetwork.com/recipes/", crawlers.NewFoodNetworkCrawler(products))

	c.CheckSources(sourcesList, func(url string, name string) (string, error) {

		source := &viewmodels.Source{
			Name: name,
			Url:  url,
		}
		_, err = backendBase.New().Post("/_internal/sources").BodyJSON(source).ReceiveSuccess(source)

		if err != nil {
			return "", err
		}

		fmt.Printf("Create source: %s, %s, with id %s", url, name, source.Id)

		return source.Id, nil
	})

	c.Run()
}
