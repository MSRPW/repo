package crawlers

import (
	"strings"
	"sync"

	"github.com/PuerkitoBio/fetchbot"
	"github.com/PuerkitoBio/goquery"

	"gitlab.com/msrpw/repo/crawler/src/helpers"
	"gitlab.com/msrpw/repo/crawler/src/parsers"
	"gitlab.com/msrpw/repo/crawler/src/viewmodels"
)

type AllrecipesCrawler struct {
	sourceId   string
	mu         sync.Mutex
	dup        map[string]bool
	parser     parsers.AllrecipesParser
	pushRecipe func(viewmodels.Recipe)
}

func NewAllrecipesCrawler(products []string) *AllrecipesCrawler {
	c := new(AllrecipesCrawler)
	c.parser = parsers.AllrecipesParser{products}
	c.dup = map[string]bool{}
	return c
}

func (self *AllrecipesCrawler) Name() string {
	return "AllRecipes.com"
}

func (self *AllrecipesCrawler) SetRecipeHandler(handler func(viewmodels.Recipe)) {
	self.pushRecipe = handler
}

func (self *AllrecipesCrawler) SetSourceId(sourceId string) {
	self.sourceId = sourceId
}

func (self *AllrecipesCrawler) HandleWebsite(ctx *fetchbot.Context, doc *goquery.Document) {
	self.mu.Lock()
	defer self.mu.Unlock()

	doc.Find("a[href]").FilterFunction(func(i int, s *goquery.Selection) bool {
		href, _ := s.Attr("href")
		return strings.HasPrefix(href, "/recipe/")

	}).Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		url, err := ctx.Cmd.URL().Parse(href)

		if helpers.HandleError(err) {
			return
		}

		url_str := url.String()

		if !self.dup[url_str] {
			self.dup[url_str] = true
			recipe, err := self.parser.ParseWebsite(url_str, self.sourceId)
			if helpers.HandleError(err) {
				return
			}

			self.pushRecipe(recipe)
		}
	})
}
