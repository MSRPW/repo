package crawlers

import (
	"github.com/PuerkitoBio/fetchbot"
	"github.com/PuerkitoBio/goquery"

	"gitlab.com/msrpw/repo/crawler/src/viewmodels"
)

type AbstractCrawler interface {
	SetSourceId(sourceId string)
	Name() string
	SetRecipeHandler(handler func(viewmodels.Recipe))
	HandleWebsite(ctx *fetchbot.Context, doc *goquery.Document)
}
