package crawlers

import (
	"fmt"
	"strings"
	"sync"

	"github.com/PuerkitoBio/fetchbot"
	"github.com/PuerkitoBio/goquery"

	"gitlab.com/msrpw/repo/crawler/src/helpers"
	"gitlab.com/msrpw/repo/crawler/src/parsers"
	"gitlab.com/msrpw/repo/crawler/src/viewmodels"
)

type FoodNetworkCrawler struct {
	sourceId   string
	mu         sync.Mutex
	dup        map[string]bool
	parser     parsers.FoodnetworkParser
	pushRecipe func(viewmodels.Recipe)
}

func NewFoodNetworkCrawler(products []string) *FoodNetworkCrawler {
	c := new(FoodNetworkCrawler)
	c.parser = parsers.FoodnetworkParser{products}
	c.dup = map[string]bool{}
	return c
}

func (self *FoodNetworkCrawler) Name() string {
	return "FoodNetwork.com"
}

func (self *FoodNetworkCrawler) SetRecipeHandler(handler func(viewmodels.Recipe)) {
	self.pushRecipe = handler
}

func (self *FoodNetworkCrawler) SetSourceId(sourceId string) {
	self.sourceId = sourceId
}

func (self *FoodNetworkCrawler) HandleWebsite(ctx *fetchbot.Context, doc *goquery.Document) {
	self.mu.Lock()
	defer self.mu.Unlock()

	doc.Find("a[href]").FilterFunction(func(i int, s *goquery.Selection) bool {
		href, _ := s.Attr("href")
		return strings.HasPrefix(href, "//www.foodnetwork.com/recipes/")

	}).Each(func(i int, s *goquery.Selection) {
		href, _ := s.Attr("href")
		url, err := ctx.Cmd.URL().Parse(href)

		fmt.Println("Found url", url)

		if helpers.HandleError(err) {
			return
		}

		url_str := url.String()

		if !self.dup[url_str] {
			self.dup[url_str] = true
			recipe, err := self.parser.ParseWebsite(url_str, self.sourceId)
			if helpers.HandleError(err) {
				return
			}

			self.pushRecipe(recipe)
		}
	})
}
