package crawlers

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/fetchbot"
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/msrpw/repo/crawler/src/helpers"
	"gitlab.com/msrpw/repo/crawler/src/viewmodels"
)

type Crawlers struct {
	crawler_map map[string]AbstractCrawler
	f           *fetchbot.Fetcher
	pushRecipe  func(viewmodels.Recipe)
}

func New() *Crawlers {

	c := new(Crawlers)
	c.crawler_map = make(map[string]AbstractCrawler)
	c.f = fetchbot.New(fetchbot.HandlerFunc(func(ctx *fetchbot.Context, res *http.Response, err error) {
		c.handleWebsite(ctx, res, err)
	}))

	return c
}

func (self *Crawlers) SetRecipeHandler(handler func(viewmodels.Recipe)) {
	self.pushRecipe = handler
}

func (self *Crawlers) AddCrawler(url string, crawler AbstractCrawler) {
	crawler.SetRecipeHandler(self.pushRecipe)
	self.crawler_map[url] = crawler
}

func (self *Crawlers) CheckSources(sourcesList *viewmodels.SourcesList, getSource func(url string, name string) (string, error)) {
	for crawler_url, crawler := range self.crawler_map {
		fmt.Println("Getting source for", crawler_url)
		sourceId, hasSourceId := GetSourceId(sourcesList, crawler_url)
		if hasSourceId == false {
			s, err := getSource(crawler_url, crawler.Name())

			if err != nil {
				fmt.Println("Couldn't get source: ", crawler_url)
				return
			}

			sourceId = s
		}

		crawler.SetSourceId(sourceId)
	}
}

func GetSourceId(sourcesList *viewmodels.SourcesList, crawler_url string) (string, bool) {
	for _, source := range sourcesList.Sources {
		if strings.HasPrefix(crawler_url, source.Url) {
			return source.Id, true
		}
	}
	return "", false
}

func (self *Crawlers) Run() {
	queue := self.f.Start()
	for url := range self.crawler_map {
		queue.SendStringGet(url)
	}
	queue.Close()
}

func (self *Crawlers) handleWebsite(ctx *fetchbot.Context, res *http.Response, err error) {
	if helpers.HandleError(err) {
		return
	}

	fmt.Printf("[%d] %s %s\n", res.StatusCode, ctx.Cmd.Method(), ctx.Cmd.URL())

	doc, err := goquery.NewDocumentFromResponse(res)

	if helpers.HandleError(err) {
		return
	}

	url := ctx.Cmd.URL().String()

	for crawler_url, crawler := range self.crawler_map {
		if strings.HasPrefix(url, crawler_url) {
			crawler.HandleWebsite(ctx, doc)
			return
		}
	}
}
