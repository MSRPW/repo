import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DataService} from '../data.service';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {SearchModule} from './search/search.module';
import {SearchComponent} from './search/search.component';

export const ROUTES: Routes = [
    {path: '', component: SearchComponent}

];


@NgModule({
    declarations: [AppComponent, HeaderComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        SearchModule,
        RouterModule.forRoot(ROUTES)
    ],
    providers: [DataService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
