import {Component} from '@angular/core';
import {DataService} from '../../data.service';
import {ProductsList, Recipe} from '../../model';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
})

export class SearchComponent {

    submittedProducts: string[];
    allProductsNames: string[];
    suggestions: string[];
    foundRecipes: Recipe[];
    up = false;

    placeholder = 'What\'s in the fridge?';
    emptyInfo = 'No products found';
    errorMessage;

    constructor(private dataService: DataService) {

    }

    searchForSuggestions(event) {
        this.getSuggestions(event.query.toLowerCase());
    }

    searchForRecipes() {
        this.up = true;
        const productsList = new ProductsList();
        productsList.products = this.submittedProducts;
        this.getRecipes(productsList);
    }

    loadAllProducts(): Promise<any> {
        const tempList = [];
        return this.dataService.getAllProducts()
            .toPromise()
            .then((result) => {
                this.errorMessage = null;
                result.forEach(asset => {
                    tempList.push(asset);
                });
                // do sth with templist if needed
            })
            .catch((error) => {
                this.processError(error);
            });
    }

    getSuggestions(substring: string): Promise<any> {
        const tempList = [];
        return this.dataService.getProductSuggestions(substring)
            .toPromise()
            .then((result) => {
                this.errorMessage = null;
                result.products.forEach(asset => {
                    tempList.push(asset);
                });
                this.suggestions = tempList;
            })
            .catch((error) => {
                this.processError(error);
            });

    }

    getRecipes(products: ProductsList): Promise<any> {
        const tempList = [];
        return this.dataService.getRecipes(products)
            .toPromise()
            .then((result) => {
                this.errorMessage = null;
                result.recipes.forEach(asset => {
                    tempList.push(asset);
                });
                this.foundRecipes = tempList;
            })
            .catch((error) => {
                this.processError(error);
            });
    }

    loadAllProductsNames(): Promise<any> {
        const tempList = [];
        return this.dataService.getAllProductsNames()
            .toPromise()
            .then((result) => {
                this.errorMessage = null;
                result.products.forEach(asset => {
                    tempList.push(asset);
                });
                this.allProductsNames = tempList;
            })
            .catch((error) => {
                this.processError(error);
            });
    }


    processError(error: any): void {
        if (error === 'Server error') {
            this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
            this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
            this.errorMessage = error;
        }
    }
}
