import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/primeng';
import {AutoCompleteModule} from 'primeng/primeng';
import {SearchComponent} from './search.component';
import {RecipeCardComponent} from '../recipe-card/recipe-card.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ButtonModule,
        AutoCompleteModule,
    ],
    exports: [
        SearchComponent
    ],
    declarations: [
        SearchComponent,
        RecipeCardComponent,
    ]
})

export class SearchModule {
}
