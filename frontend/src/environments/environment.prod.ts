export const environment = {
    production: true,
    serverURL: "metody.frondeus.pl/",
    apiURL: "api/",
    serverWithApiURL: "http://metody.frondeus.pl/api/"
};
