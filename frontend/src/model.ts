export class Product {
    name : string;
    value : string;
}

export class ProductsList {
    products : string[];
    length : number;
}

export class ProductsNameList {
    products : string[];
    length : number;
}

export class Source {
    name : string;
    url : string;
}

export class Recipe {
    id : number;
    source : Source;
    name : string;
    url : string;
    photoUrl : string;
    products : Product[];
}

export class RecipesList {
    recipes: Recipe[];
    length: number;
}
