import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { environment } from './environments/environment';
import {Product, ProductsList, ProductsNameList, Recipe, RecipesList} from "./model";

@Injectable()
export class DataService {
	private resolveSuffix: string = '?resolve=true';
	private actionUrl: string;
	private headers: Headers;
	private options;

	constructor(private http: Http) {
		this.actionUrl = environment.serverWithApiURL;
		this.headers = new Headers();
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Accept', 'application/json');
		this.options = new RequestOptions({headers: this.headers, withCredentials: true});
	}


	public getAllProducts(): Observable<Product[]> {
		console.log('GetAllProducts');
		return this.http.get(`${this.actionUrl}products/all`,this.options)
			.map(this.extractData)
			.catch(this.handleError);
	}

    public getAllProductsNames(): Observable<ProductsNameList> {
        console.log('GetAllProductsNames');
        return this.http.get(`${this.actionUrl}products/all/names`,this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    public getProductSuggestions(substring: string): Observable<ProductsNameList> {
        console.log('GetAllSuggestionsOfProduct');
        return this.http.get(`${this.actionUrl}products/suggest/${substring}`,this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    public getRecipes(products: ProductsList): Observable<RecipesList> {
        console.log('GetRecipes');
        return this.http.post(`${this.actionUrl}recipes/search`,products , this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }


	private handleError(error: any): Observable<string> {
		// Dig deeper into the error to get a better message
		let errMsg = (error.message) ? error.message :
			error.status ? `${error.status} - ${error.statusText}` : 'Server error';
		console.error(errMsg); // log to console instead
		return Observable.throw(errMsg);
	}

	private extractData(res: Response): any {
		return res.json();
	}
}
